<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexcontroller@index');

Route::get('/form', 'authcontroller@bio');

Route::post('/welcome2', 'authcontroller@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/table1', function(){
    return view('table.table1');
});