
@extends('layout.master')

@section('judul')
Form  
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form
        action="/welcome2" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Genre</label><br><br>
        <input type="radio" name="genre" value="male">Male<br>
        <input type="radio" name="genre" value="female">Female<br>
        <input type="radio" name="genre" value="other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option><br>
            <option value="amerika">Amerika</option><br>
            <option value="inggris">Inggris</option><br><br>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language" value="bahasa indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="language">English<br>
        <input type="checkbox" name="language" value="language">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="address" cols="100" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection